#I created this file
from django.http import HttpResponse
from django.shortcuts import render
def index(request):
    # a = "index.html"
    return render(request, 'index.html')

def something(request):
    return render(request, 'about.html')
    # return HttpResponse("a")

def about(request):
    return HttpResponse("Hello Universe. This is the about section")
    