#creating the board
#adding the player
#turning the turn
#checking the win
    #checking row
    #checking column
    #checking diagonals
#win
#tie

board = ['-','-','-',
         '-','-','-',
         '-','-','-']

game_still_going = True
current_player = 'X'
winner = None

def display_board():
    print(board[0] + ' | ' + board[1] + ' | ' + board[2])
    print(board[3] + ' | ' + board[4] + ' | ' + board[5]) 
    print(board[6] + ' | ' + board[7] + ' | ' + board[8])
    

def playing_game():
    global game_still_going
    display_board()
    while game_still_going:
        handle_turn(current_player)
        check_if_game_over()
        switching_turn()
    if winner == "X" or winner =="0":
        print(winner + " won")
    else:
        print("It's a tie")
    
def check_if_game_over():
    check_winner()
    check_for_tie()
    
    
    
def handle_turn(player):
    print(player + "'s turn")
    position = input("Choose the position from 1-9: ")
    valid = False
    while not valid:
        while position not in ['1','2','3','4','5','6','7','8','9']:
            position = input("Select the valid position between 1-9: ")
        
        position = int(position) - 1
        
        if board[position] == '-':
            valid = True
        else:
            print("Cant go there choose the other position")
    board[position] = player
    display_board()
     
     
     
def check_winner():
    global winner
    global check_winner
    row_winner = check_row()
    column_winner = check_column()
    diagonal_winner = check_diagonal()
    if row_winner:
        winner = row_winner
    elif column_winner:
        winner = column_winner
    elif diagonal_winner:
        winner = diagonal_winner
    else:
        winner = None

def check_row():
    global game_still_going
    row_1 = board[0] == board[1] == board[2] != '-'
    row_2 = board[3] == board[4] == board[5] != '-'
    row_3 = board[6] == board[7] == board[8] != '-'
    
    if row_1 or row_2 or row_3:
        game_still_going = False
        
    if row_1:
        return board[0] 
    elif row_2:
        return board[3] 
    elif row_3:
        return board[6] 
     # Or return None if there was no winner
    else:
     return None
 
def check_column():
    global game_still_going
    column_1 = board[0] == board[3] == board[6] != '-'
    column_2 = board[1] == board[4] == board[7] != '-'
    column_3 = board[2] == board[5] == board[8] != '-'
    
    if column_1 or column_2 or column_3:
        game_still_going = False
        
    if column_1:
        return board[0] 
    elif column_2:
        return board[1] 
    elif column_3:
        return board[2] 
     # Or return None if there was no winner
    else:
        return None
    
def check_diagonal():
    global game_still_going
    diagonal_1 = board[0] == board[4] == board[8] != '-'
    diagonal_2 = board[2] == board[4] == board[6] != '-'
    
    if diagonal_1 or diagonal_2:
        game_still_going = False
        
    if diagonal_1:
        return board[0] 
    elif diagonal_2:
        return board[2] 
     # Or return None if there was no winner
    else:
        return None

def check_for_tie():
      # Set global variables
  global game_still_going
  # If board is full
  if "-" not in board:
    game_still_going = False
    return True
  # Else there is no tie
  else:
    return False
    
def switching_turn():
    global current_player
    if current_player == "X":
        current_player = "0"
    elif current_player == "0":
        current_player = "X"
 
playing_game()
        

    