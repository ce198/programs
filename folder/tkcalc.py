import tkinter
from tkinter import * 
import re
root = Tk()
root.title("Calculator")
# enter = Entry(root, width=50, borderwidth=2)
# enter.grid(row=1, column=0, columnspan=3, padx=10, pady=10)
expression = ""

equation = StringVar() 

equation.set('') 

expression_field = Entry(root, textvariable=equation, width=10, justify='right', font=('vardana',40)) 
expression_field.grid(columnspan=3, padx=10, pady=10) 

  
def clickButton(number):
    global expression
    # global equation
    expression = expression + str(number)
    equation.set(expression)
    
def equalpress():
    # global equation
    try: 
  
        global expression 
  
        # eval function evaluate the expression 
        # and str function convert the result 
        # into string 
        total = str(eval(expression)) 
  
        equation.set(total) 
  
        # initialze the expression variable 
        # by empty string 
        expression = "" 
    # if error is generate then handle 
    # by the except block 
    except: 
  
        equation.set(" error ") 
        expression = "" 
def clear(): 
    global expression 
    expression = "" 
    equation.set("") 
    

button_1 = Button(root, text="1", padx=40, pady=20, command=lambda: clickButton("1")).grid(row=4, column=0)
button_2 = Button(root, text="2", padx=40, pady=20, command=lambda: clickButton("2")).grid(row=4, column=1)
button_3 = Button(root, text="3", padx=40, pady=20, command=lambda: clickButton("3")).grid(row=4, column=2)
button_4 = Button(root, text="4", padx=40, pady=20, command=lambda: clickButton("4")).grid(row=3, column=0)
button_5 = Button(root, text="5", padx=40, pady=20, command=lambda: clickButton("5")).grid(row=3, column=1)
button_6 = Button(root, text="6", padx=40, pady=20, command=lambda: clickButton("6")).grid(row=3, column=2)
button_7 = Button(root, text="7", padx=40, pady=20, command=lambda: clickButton("7")).grid(row=2, column=0)
button_8 = Button(root, text="8", padx=40, pady=20, command=lambda: clickButton("8")).grid(row=2, column=1)
button_9 = Button(root, text="9", padx=40, pady=20, command=lambda: clickButton("9")).grid(row=2, column=2)
button_0 = Button(root, text="0", padx=40, pady=20, command=lambda: clickButton("0")).grid(row=5, column=0)


button_clear = Button(root, text="CE", padx=40, pady=20, command=clear).grid(row=2, column=3)
button_mul = Button(root, text="*", padx=40, pady=20, command=lambda: clickButton("*")).grid(row=3, column=3)
button_sub = Button(root, text="-", padx=40, pady=20, command=lambda: clickButton("-")).grid(row=4, column=3)
button_add = Button(root, text="+", padx=40, pady=20, command=lambda: clickButton("+")).grid(row=5, column=3)
button_div = Button(root, text="/", padx=40, pady=20, command=lambda: clickButton("/")).grid(row=5, column=1)
button_equal = Button(root, text="=", padx=40, pady=20, command=equalpress).grid(row=5, column=2)
root.mainloop()