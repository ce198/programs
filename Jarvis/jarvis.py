import pyttsx3 #install this with pip install pyttsx3
import datetime
import os
import sys
import re
import webbrowser #install with pip install webbrowser
import speech_recognition as sr #install with pip install SpeechRecognition
engine = pyttsx3.init()
loop = True
print("    ----   !!!!!Hello Jay!!!!!    ----    ")

def speak(audio):
    
    engine.say(audio)
    engine.runAndWait()
speak("Hello Sir! ")

def time():
    hour = datetime.datetime.now().hour

    if hour>0 and hour<=10:
        speak("Good Morning")

    elif hour>10 and hour<=16:
        speak("Good Afternoon")

    elif hour>16 and hour<=18:
        speak("Good Evening")

    else:
        speak("Good Night")

time()
speak("Jarvis is here")
speak("What can I do for you today?") 

def myCommand():
    "listens for commands"

    r = sr.Recognizer()

    with sr.Microphone() as source:
        print('Ready...')
        r.pause_threshold = 1
        r.adjust_for_ambient_noise(source, duration=1)
        audio = r.listen(source)

    try:
        command = r.recognize_google(audio).lower()
        print('You said: ' + command + '\n')

#loop back to continue to listen for commands if unrecognizable speech is received
    except sr.UnknownValueError:
        print('Your last command couldn\'t be heard')
        command = myCommand();

    return command


def assistant(command):
    "if statements for executing commands"
    global loop


    if 'open website' in command:
        reg_ex = re.search('open website (.+)', command)
        if reg_ex:
            domain = reg_ex.group(1)
            url = 'https://www.' + domain
            webbrowser.open(url)
            print('Done!')
    
    elif 'what is the time' in command or 'what time is it' in command or 'current time' in command or 'tell me the time' in command:
        time = datetime.datetime.now().strftime("%I:%M")
        speak('The current time is ')
        speak(time)
        return
    
    elif 'how are you' in command:
        speak("I am fine. Thanks for asking ")
        speak("Hope you are also doing fine ")
        
        

    elif 'who created you' in command or 'who made you' in command or 'who is your creator' in command:
        speak('I was created by Mr. Jay Bhattarai')
        loop = False

    elif 'who are you' in command or 'do i know you' in command:
        speak("I am your virtual Assistant. And my name is jarvis")

    elif 'hello jarvis' in command or 'hey jarvis' in command or 'assistant' in command or 'call jarvis' in command:
        
        voices = engine.getProperty('voices')
        rate = engine.getProperty('rate')
        newVoiceRate = 250
        engine.setProperty('voice', voices[0].id)
        engine.setProperty('rate', newVoiceRate)
        speak("Hello Sir, jarvis is here")
        speak("What can I do for you?")
        
    elif 'jarvis'  in command:
        voices = engine.getProperty('voices')
        rate = engine.getProperty('rate')
        newVoiceRate = 250
        engine.setProperty('voice', voices[0].id)
        engine.setProperty('rate', newVoiceRate)
        speak("Yes Sir")
        

    elif 'what can you do' in command:
        speak("I can open youtube. I can send emails. I can do google search for you. I can play music. And many other things")

    elif 'i am bored' in command:
        speak('I am not a toy to play. Do whatever you want')
     
    elif 'call someone' in command:
        speak('Dude I am a computer not a mobile phone to make any calls')
        
    
    elif 'go away' in command or 'bye-bye' in command or 'get lost' in command or 'exit' in command or 'quit' in command or 'see you later' in command:
        speak("See you later boss")
        loop = False

#Calling veronica or the index 1 voice of the computer
    elif 'veronica' in command or 'hi veronica' in command or 'hello veronica' in command or 'call veronica' in command or 'talk in other voice' in command or 'other voice' in command or 'use other voice' in command or 'change your voice' in command or 'female voice' in command:
        
        voices = engine.getProperty('voices')
        rate = engine.getProperty('rate')
        newVoiceRate = 250
        engine.setProperty('voice', voices[1].id)
        engine.setProperty('rate', newVoiceRate)
        speak("Hello Sir, your veronica is here")
        speak("What can I do")

    elif 'write note' in command or 'write notes' in command:
        speak('OPening notepad ')
        os.startfile('C:\\WINDOWS\\system32\\notepad.exe')
        
        speak('Notepad is on now ')
        speak('What do you want me to write?')
        
        
    
    elif 'chrome' in command or 'google chrome' in command or 'open chrome' in command or 'open google chrome' in command: 
        speak("Google Chrome") 
        os.startfile('C:\Program Files (x86)\Google\Chrome Beta\Application\chrome.exe') 
        return
    
    elif 'open facebook' in command:
        speak("Opening facebook")
        webbrowser.open("www.facebook.com")
        
    elif 'open youtube' in command:
        speak("Opening youtube")
        webbrowser.open("www.youtube.com")
    
    elif 'what is ' in command or 'what do you mean by ' in command or 'try googling ' in command or 'try searching' in command or 'search' in command or 'search for ' in command:
        speak("Googling ")
        url = "https://www.google.co.in/search?q=" + (str(command))+ "&oq="+(str(command))+"&gs_l=serp.12..0i71l8.0.0.0.6391.0.0.0.0.0.0.0.0..0.0....0...1c..64.serp..0.0.0.UiQhpfaBsuU" + command
        webbrowser.open_new(url)
        loop = False
 
    elif  'play ' in command or 'search and play ' in command:
        speak("Playing in youtube ")
        url = 'http://www.youtube.com/results?search_query=' + command 
        webbrowser.open(url)
        loop = False
        
    else:
        print("Please say that again")
        speak("Please say that again")


#loop to continue executing multiple commands
while loop:
    assistant(myCommand())