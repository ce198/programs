import tkinter
from tkinter import *
from tkinter import ttk
parent = Tk()


#creating a spinbox
my_spinbox = Spinbox(parent, from_=0, to=50, increment=2, width=50, border=10, borderwidth=0).pack()


#creating a combobox
#it uses ttk
my_combobox = ttk.Combobox(parent,text='Click me', textvariable='Click me', values=['Option 1', 'Option 2', 'Option 3']).pack()

#creating a checkbox
my_checkbox = Checkbutton(parent, text='Click me to check').pack()


#creating a text wizard
# my_textbox = Text(parent)
# my_textbox.insert('This is not a text')
# my_textbox.insert('I am kidding')

# my_textbox.pack()
parent.mainloop()