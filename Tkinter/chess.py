from tkinter import * 

chess = Tk()

chess.title('Chess- Game')
chess.geometry('640x560')
chess.resizable(0,0)


j=0
i=1
buttonup_pawn1 = Button(chess, text='P', fg='black', bg="grey", borderwidth=0,width=10, height=4, command=lambda: pawn(buttonup_pawn1))
buttonup_pawn1.grid(row=i,column=j)
buttonup_pawn2 = Button(chess, text='P', fg='black', bg="white", borderwidth=0,width=10, height=4, command=lambda: pawn(buttonup_pawn2))
buttonup_pawn2.grid(row=i,column=j+1)
buttonup_pawn3 = Button(chess, text='P', fg='black', bg="grey", borderwidth=0,width=10, height=4, command=lambda: pawn(buttonup_pawn3))
buttonup_pawn3.grid(row=i,column=j+2)
buttonup_pawn4 = Button(chess, text='P', fg='black', bg="white", borderwidth=0,width=10, height=4, command=lambda: pawn(buttonup_pawn4))
buttonup_pawn4.grid(row=i,column=j+3)
buttonup_pawn5 = Button(chess, text='P', fg='black', bg="grey", borderwidth=0,width=10, height=4, command=lambda: pawn(buttonup_pawn5))
buttonup_pawn5.grid(row=i,column=j+4)
buttonup_pawn6 = Button(chess, text='P', fg='black', bg="white", borderwidth=0,width=10, height=4, command=lambda: pawn(buttonup_pawn6))
buttonup_pawn6.grid(row=i,column=j+5)
buttonup_pawn7 = Button(chess, text='P', fg='black', bg="grey", borderwidth=0,width=10, height=4, command=lambda: pawn(buttonup_pawn7))
buttonup_pawn7.grid(row=i,column=j+6)
buttonup_pawn8 = Button(chess, text='P', fg='black', bg="white", borderwidth=0,width=10, height=4, command=lambda: pawn(buttonup_pawn8))
buttonup_pawn8.grid(row=i,column=j+7)



j=0
i=0
buttonup_hathi1 = Button(chess, text='H', fg='black', bg="white", borderwidth=0,width=10, height=4, command=lambda: hathi(buttonup_hathi1))
buttonup_hathi1.grid(row=i,column=j)
buttonup_ghoda1 = Button(chess, text='G', fg='black', bg="grey", borderwidth=0,width=10, height=4, command=lambda: ghoda(buttonup_ghoda1))
buttonup_ghoda1.grid(row=i,column=j+1)
buttonup_plus1 = Button(chess, text='+', fg='black', bg="white", borderwidth=0,width=10, height=4, command=lambda: plus(buttonup_plus1))
buttonup_plus1.grid(row=i,column=j+2)
buttonup_queen = Button(chess, text='Q', fg='black', bg="grey", borderwidth=0,width=10, height=4, command=lambda: queen(buttonup_queen))
buttonup_queen.grid(row=i,column=j+3)
buttonup_king = Button(chess, text='K', fg='black', bg="white", borderwidth=0,width=10, height=4, command=lambda: king(buttonup_king))
buttonup_king.grid(row=i,column=j+4)
buttonup_plus2 = Button(chess, text='+', fg='black', bg="grey", borderwidth=0,width=10, height=4, command=lambda: plus(buttonup_plus2))
buttonup_plus2.grid(row=i,column=j+5)
buttonup_ghoda2 = Button(chess, text='G', fg='black', bg="white", borderwidth=0,width=10, height=4, command=lambda: ghoda(buttonup_ghoda2))
buttonup_ghoda2.grid(row=i,column=j+6)
buttonup_hathi2 = Button(chess, text='H', fg='black', bg="grey", borderwidth=0,width=10, height=4, command=lambda: hathi(buttonup_hathi2))
buttonup_hathi2.grid(row=i,column=j+7)

j=0
i=2
button = Button(chess, text=' ', bg="white",command=lambda: otherbutton(button), borderwidth=0,width=10, height=4)
button.grid(row=i,column=j)
button = Button(chess, text=' ', bg="grey",command=lambda: otherbutton(button), borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+1)
button = Button(chess, text=' ', bg="white",command=lambda: otherbutton(button), borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+2)
button = Button(chess, text=' ', bg="grey",command=lambda: otherbutton(button), borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+3)
button = Button(chess, text=' ', bg="white",command=lambda: otherbutton(button), borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+4)
button = Button(chess, text=' ', bg="grey",command=lambda: otherbutton(button), borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+5)
button = Button(chess, text=' ', bg="white",command=lambda: otherbutton(button), borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+6)
button = Button(chess, text=' ', bg="grey",command=lambda: otherbutton(button), borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+7)



j=0
i=3
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+1)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+2)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+3)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+4)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+5)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+6)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+7)

j=0
i=4
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+1)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+2)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+3)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+4)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+5)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+6)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+7)



j=0
i=5
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+1)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+2)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+3)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+4)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+5)
button = Button(chess, bg="grey", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+6)
button = Button(chess, bg="white", borderwidth=0,width=10, height=4)
button.grid(row=i,column=j+7)

j=0
i=7
button_pawn1 = Button(chess, text='P', fg='red', bg="white", borderwidth=0,width=10, height=4, command=lambda: pawn(button_pawn1))
button_pawn1.grid(row=i,column=j)
button_pawn2 = Button(chess, text='P', fg='red', bg="grey", borderwidth=0,width=10, height=4, command=lambda: pawn(button_pawn2))
button_pawn2.grid(row=i,column=j+1)
button_pawn3 = Button(chess, text='P', fg='red', bg="white", borderwidth=0,width=10, height=4, command=lambda: pawn(button_pawn3))
button_pawn3.grid(row=i,column=j+2)
button_pawn4 = Button(chess, text='P', fg='red', bg="grey", borderwidth=0,width=10, height=4, command=lambda: pawn(button_pawn4))
button_pawn4.grid(row=i,column=j+3)
button_pawn5 = Button(chess, text='P', fg='red', bg="white", borderwidth=0,width=10, height=4, command=lambda: pawn(button_pawn5))
button_pawn5.grid(row=i,column=j+4)
button_pawn6 = Button(chess, text='P', fg='red', bg="grey", borderwidth=0,width=10, height=4, command=lambda: pawn(button_pawn6))
button_pawn6.grid(row=i,column=j+5)
button_pawn7 = Button(chess, text='P', fg='red', bg="white", borderwidth=0,width=10, height=4, command=lambda: pawn(button_pawn7))
button_pawn7.grid(row=i,column=j+6)
button_pawn8 = Button(chess, text='P', fg='red', bg="grey", borderwidth=0,width=10, height=4, command=lambda: pawn(button_pawn8))
button_pawn8.grid(row=i,column=j+7)



j=0
i=8
button_hathi1 = Button(chess, text='H', fg='red', bg="grey", borderwidth=0,width=10, height=4, command=lambda: hathi(button_hathi1))
button_hathi1.grid(row=i,column=j)
button_ghoda1 = Button(chess, text='G', fg='red', bg="white", borderwidth=0,width=10, height=4, command=lambda: ghoda(button_ghoda1))
button_ghoda1.grid(row=i,column=j+1)
button_plus1 = Button(chess, text='+', fg='red', bg="grey", borderwidth=0,width=10, height=4, command=lambda: plus(button_plus1))
button_plus1.grid(row=i,column=j+2)
button_queen = Button(chess, text='Q', fg='red', bg="white", borderwidth=0,width=10, height=4, command=lambda: queen(button_queen))
button_queen.grid(row=i,column=j+3)
button_king = Button(chess, text='K', fg='red', bg="grey", borderwidth=0,width=10, height=4, command=lambda: king(button_king))
button_king.grid(row=i,column=j+4)
button_plus2 = Button(chess, text='+', fg='red', bg="white", borderwidth=0,width=10, height=4, command=lambda: plus(button_plus2))
button_plus2.grid(row=i,column=j+5)
button_ghoda2 = Button(chess, text='G', fg='red', bg="grey", borderwidth=0,width=10, height=4, command=lambda: ghoda(button_ghoda2))
button_ghoda2.grid(row=i,column=j+6)
button_hathi2 = Button(chess, text='H', fg='red', bg="white", borderwidth=0,width=10, height=4, command=lambda: hathi(button_hathi2))
button_hathi2.grid(row=i,column=j+7)




buttonClick = True

def pawn(buttons):
    global buttonClick
    if buttons['text'] == 'P' and buttonClick == True:
        buttons['text'] = ''
        buttonClick = False
        
        
def hathi(buttons):
    global buttonClick
    if buttons['text'] == 'H' and buttonClick == True:
        buttons['text'] = ''
        buttonClick = False
        
def ghoda(buttons):
    global buttonClick
    if buttons['text'] == 'G' and buttonClick == True:
        buttons['text'] = ''
        buttonClick = False
        
def plus(buttons):
    global buttonClick
    if buttons['text'] == '+' and buttonClick == True:
        buttons['text'] = ''
        buttonClick = False
        
def king(buttons):
    global buttonClick
    if buttons['text'] == 'K' and buttonClick == True:
        buttons['text'] = ''
        buttonClick = False
        
    
def queen(buttons):
    global buttonClick
    if buttons['text'] == 'Q' and buttonClick == True:
        buttons['text'] = ''
        buttonClick = False
        
        
def otherbutton(buttons):
    global buttonClick
    if buttons['text'] == " " and buttonClick == False:
        buttons['text'] = 'P'
        buttonClick = True
buttons = StringVar()

chess.mainloop()