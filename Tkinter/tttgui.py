import tkinter 
from tkinter import *
from tkinter.messagebox import showinfo
game = Tk()
game.geometry('303x390')
game.configure(background='black')
game.title("Tic-Tac-Toe")
game.resizable(0,0)
bclick = True
clicked = 0
Xwin = 0
Owin = 0

#Assigning the click to the buttons
def btnClick(buttons):
    global bclick, clicked
    if buttons["text"] == " " and bclick == True:
        buttons["text"] = "X"
        bclick = False
        clicked += 1
        checkingwin()
        
    if buttons["text"] == " " and bclick == False:
        buttons["text"] = "0"
        bclick = True
        clicked += 1
        checkingwin()
        
#Restarting function- If I click on the restart then the buttons will be blank, 
#clicked is set to 0 so that the program stops after restart is clicked 
def restart():
    global bclick, clicked
    bclick = True
    clicked = 0    
    button1['text'] = ' '
    button2['text'] = ' '
    button3['text'] = ' '
    button4['text'] = ' '
    button5['text'] = ' '
    button6['text'] = ' '
    button7['text'] = ' '
    button8['text'] = ' '
    button9['text'] = ' '
    
#To check the win, I used the RULE of the game TICTACTOE    
def checkingwin():
    global Xwin, Owin
    if (
       button1['text'] == button2['text'] == button3['text'] == 'X' or
       button4['text'] == button5['text'] == button6['text'] == 'X' or
       button7['text'] == button8['text'] == button9['text'] == 'X' or
       button1['text'] == button4['text'] == button7['text'] == 'X' or
       button2['text'] == button5['text'] == button8['text'] == 'X' or
       button1['text'] == button5['text'] == button9['text'] == 'X' or
       button7['text'] == button5['text'] == button3['text'] == 'X'
       ):
        print("X is winner")
        tkinter.messagebox.showinfo("Tic-Tac-Toe", "X is Winner")
        Xwin += 1
        print(Xwin)
        
        
        
    elif (
       button1['text'] == button2['text'] == button3['text'] == '0' or
       button4['text'] == button5['text'] == button6['text'] == '0' or
       button7['text'] == button8['text'] == button9['text'] == '0' or
       button1['text'] == button4['text'] == button7['text'] == '0' or
       button2['text'] == button5['text'] == button8['text'] == '0' or
       button1['text'] == button5['text'] == button9['text'] == '0' or
       button7['text'] == button5['text'] == button3['text'] == '0'
       ):
        print("0 is winner")
        tkinter.messagebox.showinfo("Tic-Tac-Toe", "0 is Winner")
        Owin += 1
        print(Owin)
        
    elif (clicked == 9):
        tkinter.messagebox.showinfo("Tic-Tac-Toe", "It's a tie")
        
buttons = StringVar()
player_1 = Label(game, text="Player 1 (X)", font=("Chiller",18), fg='red', bg='black').grid(row=0,column=0)
player_2 = Label(game, text="Player 2 (0)", font=("Chiller",18), fg='red', bg='black').grid(row=0,column=2)


#Creating all the buttons
button1=Button(game, text=" ", font=("Chiller",18), fg='black',bg='#ffffff', width=10, height=3, borderwidth=0, command=lambda: btnClick(button1))
button2=Button(game, text=" ", font=("Chiller",18), fg='black',bg='#ffffff', width=10, height=3, borderwidth=0, command=lambda: btnClick(button2))
button3=Button(game, text=" ", font=("Chiller",18), fg='black',bg='#ffffff', width=10, height=3, borderwidth=0, command=lambda: btnClick(button3))

button4=Button(game, text=" ", font=("Chiller",18), fg='black',bg='#ffffff', width=10, height=3, borderwidth=0, command=lambda: btnClick(button4))
button5=Button(game, text=" ", font=("Chiller",18), fg='black',bg='#ffffff', width=10, height=3, borderwidth=0, command=lambda: btnClick(button5))
button6=Button(game, text=" ", font=("Chiller",18), fg='black',bg='#ffffff', width=10, height=3, borderwidth=0, command=lambda: btnClick(button6))

button7=Button(game, text=" ", font=("Chiller",18), fg='black',bg='#ffffff', width=10, height=3, borderwidth=0, command=lambda: btnClick(button7))
button8=Button(game, text=" ", font=("Chiller",18), fg='black',bg='#ffffff', width=10, height=3, borderwidth=0, command=lambda: btnClick(button8))
button9=Button(game, text=" ", font=("Chiller",18), fg='black',bg='#ffffff', width=10, height=3, borderwidth=0, command=lambda: btnClick(button9))

# button10=Button(game, text="X=", font=("Times",12), fg='white',bg='black', width=10, height=3, borderwidth=0)
button11=Button(game, text="RESTART", font=("Chiller",18), fg='blue',bg='#ffffff', width=10, height=2, borderwidth=0, command=restart)
# button12=Button(game, text="0=", font=("Times",12), fg='white',bg='black', width=10, height=3, borderwidth=0)


#Giving the proper position to all the buttons
button1.grid(row=1,column=0,ipadx=1,pady=1)
button2.grid(row=1,column=1,ipadx=1,pady=1)
button3.grid(row=1,column=2,ipadx=1,pady=1)

button4.grid(row=2,column=0,ipadx=1,pady=1)
button5.grid(row=2,column=1,ipadx=1,pady=1)
button6.grid(row=2,column=2,ipadx=1,pady=1)

button7.grid(row=3,column=0,ipadx=1,pady=1)
button8.grid(row=3,column=1,ipadx=1,pady=1)
button9.grid(row=3,column=2,ipadx=1,pady=1)

# button10.grid(row=4,column=0,ipadx=1,pady=1)
button11.grid(row=4,column=1,ipadx=1,pady=1)
# button12.grid(row=4,column=2,ipadx=1,pady=1)

#This will help to end the program only after the user close the program. It will not automatically 
# terminate    
game.mainloop()