from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
import json
from datetime import datetime

with open("config.json", 'r') as c:
    params = json.load(c)["params"]

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = params['local_uri']
db = SQLAlchemy(app)


class Contacts(db.Model):
    sn = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    email = db.Column(db.String(50), nullable=False)
    phone = db.Column(db.String(50), nullable=False)
    message = db.Column(db.String(120), nullable=False)
    date = db.Column(db.String(20), nullable=True)


class Posts(db.Model):
    sn = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(20), nullable=False)
    slug = db.Column(db.String(50), nullable=False)
    content = db.Column(db.String(50), nullable=False)
    date = db.Column(db.String(20), nullable=True)


@app.route('/')
@app.route('/main/')
def main():
    return render_template('mainbody.html', params=params)


@app.route('/contact/', methods=['GET', 'POST'])
def contact():
    if (request.method == 'POST'):
        # Add database entry
        names = request.form.get('name')
        emails = request.form.get('email')
        phones = request.form.get('phone_num')
        messages = request.form.get('message')
        entry = Contacts(name=names, email=emails, phone=phones,
                         message=messages, date=datetime.now())
        db.session.add(entry)
        db.session.commit()
    return render_template('contact.html', params=params)



@app.route('/content/', methods=['GET', 'POST'])
def content():
    if (request.method == 'POST'):
        # Add database entry
        title = request.form.get('title')
        slug = request.form.get('slug')
        content = request.form.get('content')
        enterpost = Posts(title=title, slug=slug,
                         content=content, date=datetime.now())
        db.session.add(enterpost)
        db.session.commit()
    return render_template('content.html', params=params)


@app.route('/posts/<string:posts_slug>', methods=['GET'])
def posts_route(posts_slug):
    post = Posts.query.filter_by(slug=posts_slug).first()
    return render_template('posts.html', params=params, post=post)


@app.route('/python/')
def python():
    return render_template('pythonpage.html', params=params)


@app.route('/javascript/')
def javascript():
    return render_template('javascriptpage.html', params=params)



@app.route('/about/')
def about():
    return render_template('aboutus.html', params=params)


@app.route('/help/')
def help():
    return render_template('help.html', params=params)


app.run(debug=True)
