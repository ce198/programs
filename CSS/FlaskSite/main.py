from flask import *
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:@localhost/blog'
db = SQLAlchemy(app)

class Contacts(db.Model):
    sn = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(50), nullable=False)
    message = db.Column(db.String(200), nullable=False)

@app.route('/')
def home():
    return render_template('main.html')

@app.route('/contact', methods=['GET','POST'])
def contact():
    if method == 'POST':
        name
    return render_template('contact.html')

@app.route('/index')
def index():
    return render_template('main.html')

@app.route('/about')
def about():
    return render_template('about.html')


app.run(debug=True)
