class Salary:
    def __init__(self,pay,bonus):
        self.pay = pay 
        self.bonus = bonus
    
    def total_salary(self):
        return self.pay*12 + self.bonus

class Employee:
    def __init__(self,name,work,pay,bonus):
            self.name = name
            self.work = work
            self.obj_Salary = Salary(pay, bonus)

    def annual_salary(self):
        return self.obj_Salary.total_salary()
    
emp = Employee('Jay', 'SE', 1500, 20)
print(emp.name,emp.work,emp.obj_Salary.pay,emp.obj_Salary.bonus)
print(emp.annual_salary())

